#! /usr/bin/env python3

import argparse
from bs4 import BeautifulSoup
from config import USER, PASSWD, OUTPUT_DIR, LEAGUE_ID, NUMBER_OF_TEAMS,\
    USER_TEAM_NUMBER, LEAGUE_START
import csv
from datetime import date as dtdate
from datetime import datetime, timedelta
from math import floor
import requests
from requests.auth import HTTPBasicAuth
import time
from unidecode import unidecode


class Browser(object):
    """ Simple webbrowser for the yahoo fantasy site """
    def __init__(self):
        self.base_url = "http://baseball.fantasysports.yahoo.com/b1/" +\
            str(LEAGUE_ID)
        self.session = requests.Session()
        self.login()

    def login(self):
        r = self.session.post("https://login.yahoo.com/config/login?",
                {"login": USER, "passwd": PASSWD}, allow_redirects=True)
        if r.status_code != 200:
            raise Exception("Failed login")

    def get(self, team, date = None):
        url = self.base_url + '/' + str(team)
        if date:
            url += '/team?date=' + date
        return self.session.get(url)

class Scraper(object):
    def __init__(self, *args, range_ = True, filename = None,
            include_today = False, verbose = False):
        self.verbose = verbose
        self.browser = Browser()
        self.batters = []
        self.pitchers = []
        shift = timedelta(0) if include_today else timedelta(1)
        if len(args) == 0:
            self.dates = [(dtdate.today() - shift).isoformat()]
        else:
            try:
                dt_list =\
                    [datetime.strptime(d, '%Y-%m-%d').date() for d in args]
            except ValueError as e:
                raise e
            if len(dt_list) == 1 and dt_list[0] != dtdate.today() - shift:
                dt_list.append(dtdate.today() - shift)
            dt_list.sort()
            if range_:
                self.dates = []
                for n in range(int ((dt_list[-1] - dt_list[0]).days) + 1):
                    self.dates.append(
                        (dt_list[0] + timedelta(n)).isoformat()
                    )
            else:
                self.dates = list(args)
        if filename:
            self.filename = filename
        elif len(self.dates) == 1:
            self.filename = self.dates[0].replace('-', '')
        else:
            self.filename = self.dates[0].replace('-', '') + '-' +\
                self.dates[-1].replace('-', '')
        self.scrape()


    def scrape(self):
        for date in self.dates:
            for team in range(1, int(NUMBER_OF_TEAMS) + 1):
                time.sleep(2)
                self.scrape_team(team, date)

    def scrape_team(self, team, date):
        res = self.browser.get(team, date)
        if self.verbose:
            print('Team: {0}; date: {1}\n'.format(team, date))
        team_data = ScrapeRosterPage(res.text, team, date)
        self.batters.extend(team_data.batters)
        self.pitchers.extend(team_data.pitchers)

    def to_csv(self):
        with open(OUTPUT_DIR + self.filename + '-batters.csv', 'w',
                newline = '') as f:
            writr = csv.DictWriter(f, [
                'date',
                'team',
                'week',
                'pos',
                'unique',
                'player',
                'H',
                'AB',
                'R',
                '2B',
                'HRS',
                'RBI',
                'SB',
                'BB',
                'AVG',
                'SLAM'
                ])
            writr.writeheader()
            for batter in self.batters:
                writr.writerow(batter)
        with open(OUTPUT_DIR + self.filename + '-pitchers.csv', 'w',
                newline = '') as f:
            writr = csv.DictWriter(f, [
                'date',
                'team',
                'week',
                'pos',
                'unique',
                'player',
                'IP',
                'W',
                'SHO',
                'SV',
                'K',
                'ERA',
                'WHIP',
                'QS'
                ])
            writr.writeheader()
            for pitcher in self.pitchers:
                writr.writerow(pitcher)


class ScrapeRosterPage(object):
    """ Parse html for player stats """
    def __init__(self, html, team, date):
        soup = BeautifulSoup(html)
        self.batters = []
        self.pitchers = []
        self.team = team
        self.date = date
        self.week = self._get_week()
        # Table has extra columns for opposing teams so some indices
        # need to be adjusted
        self.team_adj = 2 if int(team) == int(USER_TEAM_NUMBER) else 0
        batter_table = soup.find(id = 'statTable0').find('tbody')
        for tr in batter_table.find_all('tr'):
            self._parse_batter(tr)
        pitcher_table = soup.find(id = 'statTable1').find('tbody')
        for tr in pitcher_table.find_all('tr'):
            self._parse_pitcher(tr)

    def _get_week(self):
        dtday = datetime.strptime(self.date, '%Y-%m-%d').date()
        td = (dtday - datetime.strptime(LEAGUE_START, '%Y-%m-%d').date()).days
        return floor(td/7) + 1

    def _parse_batter(self, tr):
        if tr.find('div', 'emptyplayer'):
            print("Empty roster spot")
            return
        tds = tr.find_all('td')
        player_td = tds[1].find_all('a')[-1]
        hits_ab = tds[9 - self.team_adj].text.split('/')
        f = lambda x: x if x != '-' else ''
        player_info = {
            'team': self.team,
            'date': self.date,
            'week': self.week,
            'pos': tds[0].text,
            'unique': player_td.attrs['href'].split('/')[-1],
            'player': unidecode(player_td.text),
            'H': f(hits_ab[0]),
            'AB': f(hits_ab[1]),
            'R': f(tds[10 - self.team_adj].text),
            '2B': f(tds[11 - self.team_adj].text),
            'HRS': f(tds[12 - self.team_adj].text),
            'RBI': f(tds[13 - self.team_adj].text),
            'SB': f(tds[14 - self.team_adj].text),
            'BB': f(tds[15 - self.team_adj].text),
            'AVG': f(tds[16 - self.team_adj].text),
            'SLAM': f(tds[17 - self.team_adj].text),
        }
        self.batters.append(player_info)

    def _parse_pitcher(self, tr):
        if tr.find('div', 'emptyplayer'):
            print("Empty roster spot")
            return
        tds = tr.find_all('td')
        player_td = tds[1].find_all('a')[-1]
        f = lambda x: x if x != '-' else ''
        player_info = {
            'team': self.team,
            'date': self.date,
            'week': self.week,
            'pos': tds[0].text,
            'unique': player_td.attrs['href'].split('/')[-1],
            'player': player_td.text,
            'IP': f(tds[9 - self.team_adj].text),
            'W': f(tds[10 - self.team_adj].text),
            'SHO': f(tds[11 - self.team_adj].text),
            'SV': f(tds[12 - self.team_adj].text),
            'K': f(tds[13 - self.team_adj].text),
            'ERA': f(tds[14 - self.team_adj].text),
            'WHIP': f(tds[15 - self.team_adj].text),
            'QS': f(tds[16 - self.team_adj].text),
        }
        self.pitchers.append(player_info)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('dates', help='Dates which to parse (defaults to yesterday; must use "YYYY-MM-DD" format)', nargs='*')
    parser.add_argument('-o', '--output',
        help="Output file (will be overwritten)",
        default = None)
    parser.add_argument('-r', '--range',
        help="Dates define a range (default is true)",
        default=True,
        action="store_true")
    parser.add_argument('-R', '--no-range',
        help="Scrape only dates explicitly passed as positional arguments (do not use in conjunctin with -r)",
        default=False,
        action="store_true")
    parser.add_argument('-i', '--include-today',
        help="Include the current day in range calculations (default is false)",
        default=False,
        action="store_true")
    parser.add_argument('-v', '--verbose',
        help="Show team and date progress while scraping",
        default=False,
        action="store_true")
    args = parser.parse_args()

    if args.range and args.no_range:
        raise ValueError("Ambiguous range definition. Cannot pass -r and -R simultaneously.")
    range_ = False if args.no_range else True
    scraper = Scraper(*args.dates,
        range_ = range_,
        filename = args.output,
        include_today = args.include_today,
        verbose = args.verbose)


    scraper.to_csv()


