# Yahoo Fantasy Scraper

A simple command line script to scrape day to day player stats for a yahoo fantasy baseball league.

Tested for `python 3.4`.

Requires `beautifulsoup4` and `python-requests`.

## Easy Installation

1. `git clone` this repository.
2. `virtualenv .venv` -- tested for python 3.4
3. `pip install -r requirements.txt`

## Usage

Copy `config-template.py` to `config.py` and fill in your local settings.

    usage: scrape.py [-h] [-o OUTPUT] [-r] [-R] [-i] [-v] [dates [dates ...]]

    positional arguments:
      dates                 Dates which to parse (defaults to yesterday; must use
                            "YYYY-MM-DD" format)

    optional arguments:
      -h, --help            show this help message and exit
      -o OUTPUT, --output OUTPUT
                            Output file (will be overwritten)
      -r, --range           Dates define a range (default is true)
      -R, --no-range        Scrape only dates explicitly passed as positional
                            arguments (do not use in conjunctin with -r)
      -i, --include-today   Include the current day in range calculations (default
                            is false)
      -v, --verbose         Show team and date progress while scraping


Output will be written in csv format to `OUTPUT_DIR/yyyymmdd-[batters,pitchers].csv` or `OUTPUT_DIR/yyyymmdd-yyyymmdd-[batters,pitchers].csv`, depending on your settings. You can rename the base of the file name with `-o` (see above).

## Examples

Scrape yesterday's game data:

    python scrape.py
    
Scrape today's game data:

    python scrape.py -i
    
Scrape all game data for the days between 3/31/2014 and yesterday:

    python scrape.py 2014-03-31
    
Scrape all game data for the days between 3/31/2014 and today:

    python scrape.py -i 2014-03-31
    
Scrape all game data for the days between 3/31/2014 and 4/05/2014:

    python scrape.py 2014-03-31 2014-05-04
    
Scrape game data for the days 3/31/2014, 4/05/2014, and 5/01/2014:
    
    python scrape.py -R 2014-03-31 2014-04-05 2014-05-01



